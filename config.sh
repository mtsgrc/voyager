#!/usr/bin/env bash

# Declarations

# --------------------------------------------------
# ---------------- Static configs ------------------
# --------------------------------------------------
MAX_CAMS=10

IP_SERVER=192.168.40.100
IP_GATEWAY=192.168.40.1
IP_MASK=255.255.255.0
IP_INITCAMS=192.168.40.200

CAMS_TYPES=(vivotek5315 aircam_dome)

declare -A CAMS_TYPES_MACADDRESSES=( ["vivotek5315"]="0002D1" ["aircam_dome"]="DC9FDB" )
declare -A CAMS_TYPES_MAINSTREAM=( ["vivotek5315"]="/live.sdp" ["aircam_dome"]="/live/ch00_0" )
declare -A CAMS_TYPES_PROTOCOL=( ["vivotek5315"]="rtsp" ["aircam_dome"]="rtsp" )
declare -A CAMS_TYPES_PORT=( ["vivotek5315"]="554" ["aircam_dome"]="554" )

# --------------------------------------------------
# ---------------- Variable configs ----------------
# --------------------------------------------------

CAMS_INSTALLED=(aircam_dome aircam_dome vivotek5315)

CAMS_COUNT=${#CAMS_INSTALLED[@]}
