#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SCRIPT_INITDATE=$(date +"%Y%m%d-%H%M%S")
STR_TIME="date +%H:%M:%S.%3N"

source "${DIR}/config.sh"

echo "================ [ Voyager daemonized ] ================"
echo $($STR_TIME)" > Iniciando aplicacion de grabación..."
echo $($STR_TIME)" > Leyendo configuraciones..."

# ---------------------------------------------------------------------------------
# ------------------- Mostrando configuraciones de aplicacion ---------------------
# ---------------------------------------------------------------------------------

actCAM=0
camFOUNDS=0
echo -e $($STR_TIME)" > \tBuscando cámaras conectadas..."

for i in $(seq 1 $MAX_CAMS); do

  TEST_IP="echo \$IP_INITCAMS | awk -F. '\$4=\$4+$i' OFS=".""
  TEST_IP=$(eval $TEST_IP)

  ping -c1 $TEST_IP 2>/dev/null 1>/dev/null
  if [ "$?" = 0 ]; then
    camFOUNDS=$(( $camFOUNDS + 1 ))
    macFOUND=$(arp -a ${TEST_IP} | awk '{print $4}')
    echo -e $($STR_TIME)" > \t\t$camFOUNDS: ${TEST_IP} - ${macFOUND}"
  fi

done

exit 0

actCAM=0
echo -e $($STR_TIME)" > \tCámaras configuradas: ${CAMS_COUNT}"

for cam_installed in "${!CAMS_INSTALLED[@]}"; do

  actCAM=$(( $actCAM + 1 ))
  THISCAM_NAME="CAM${actCAM}"
  THISCAM_IP="echo \$IP_INITCAMS | awk -F. '\$4=\$4+$actCAM' OFS=".""
  THISCAM_IP=$(eval $THISCAM_IP)
  THISCAM_FILESTREAM="${CAMS_TYPES_PROTOCOL[${CAMS_INSTALLED[cam_installed]}]}://${THISCAM_IP}:${CAMS_TYPES_PORT[${CAMS_INSTALLED[cam_installed]}]}${CAMS_TYPES_MAINSTREAM[${CAMS_INSTALLED[cam_installed]}]}"
  echo -e $($STR_TIME)" > \t\t$actCAM: ${THISCAM_NAME} - ${CAMS_INSTALLED[cam_installed]} - ${THISCAM_IP} - ${THISCAM_FILESTREAM}"

done

# ---------------------------------------------------------------------------------
# ------------------ Comprobando conexion a las camaras con ping ------------------
# ---------------------------------------------------------------------------------
actCAM=0
echo -e $($STR_TIME)" >"
echo -e $($STR_TIME)" > \tComprobando conexión de cámaras: ${CAMS_COUNT}"

for cam_installed in "${!CAMS_INSTALLED[@]}"; do

  actCAM=$(( $actCAM + 1 ))
  THISCAM_NAME="CAM${actCAM}"
  THISCAM_IP="echo \$IP_INITCAMS | awk -F. '\$4=\$4+$actCAM' OFS=".""
  THISCAM_IP=$(eval $THISCAM_IP)
  THISCAM_FILESTREAM="${CAMS_TYPES_PROTOCOL[${CAMS_INSTALLED[cam_installed]}]}://${THISCAM_IP}:${CAMS_TYPES_PORT[${CAMS_INSTALLED[cam_installed]}]}${CAMS_TYPES_MAINSTREAM[${CAMS_INSTALLED[cam_installed]}]}"

  ping -c1 $THISCAM_IP 2>/dev/null 1>/dev/null
  if [ "$?" = 0 ]; then
    echo -e $($STR_TIME)" > \t\t$actCAM: ${THISCAM_NAME} - ${CAMS_INSTALLED[cam_installed]} - ${THISCAM_IP} - ACCESIBLE"
  else
    echo -e $($STR_TIME)" > \t\t$actCAM: ${THISCAM_NAME} - ${CAMS_INSTALLED[cam_installed]} - ${THISCAM_IP} - NO ACCESIBLE"
  fi

done
